import type { VerticalNavItems } from '@/@layouts/types'

export default [
  {
    title: 'Home',
    to: { name: 'index' },
    icon: { icon: 'tabler-smart-home' },
  },
  {
    title: 'Namespaces',
    to: { name: 'namespaces' },
    icon: { icon: 'tabler-file' },
  },
  {
    title: 'Deployments',
    to: { name: 'deployments' },
    icon: { icon: 'tabler-file' },
  },
] as VerticalNavItems
