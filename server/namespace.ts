import { CoreV1Api } from '@kubernetes/client-node';

export class Namespaces {
  private api: CoreV1Api;

  constructor(api: CoreV1Api) {
    this.api = api;
  }

  async getNamespaces(): Promise<any> {
    const { body: { items } } = await this.api.listNamespace();

    return items
      .map(ns => ns.metadata?.name)
    return
  }
}

module.exports = { Namespaces }