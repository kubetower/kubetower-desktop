import { AppsV1Api, V1Deployment } from '@kubernetes/client-node';

export class Deployments {
  private api: AppsV1Api;

  constructor(api: AppsV1Api) {
    this.api = api;
  }

  async getDeploymentsByNamespace(ns): Promise<V1Deployment[]> {
    const { body: { items } } = await this.api.listNamespacedDeployment(ns)

    return items
  }

  async getDeployment(ns, deployName): Promise<V1Deployment> {
    const { body } = await this.api.readNamespacedDeployment(deployName,ns)

    return body
  }
}

module.exports = { Deployments }