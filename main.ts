import { AppsV1Api, CoreV1Api, KubeConfig } from "@kubernetes/client-node";
import { BrowserWindow, app, ipcMain } from "electron";
import { Deployments } from "./server/deployment";
import { Namespaces } from "./server/namespace";

async function createWindow() {
  // Crie uma janela de navegador.
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    fullscreen: true,
    webPreferences: {
      contextIsolation: false,
      nodeIntegration: true,
      nodeIntegrationInWorker: true
    },
  });

  win.loadFile("dist/index.html");

  win.webContents.openDevTools();


  const kubeConfig = new KubeConfig();
  kubeConfig.loadFromDefault();

  // Crie uma instância do objeto CoreV1Api usando a conexão com o Kubernetes
  const coreApi = kubeConfig.makeApiClient(CoreV1Api);
  const appsApi = kubeConfig.makeApiClient(AppsV1Api);
  
  const namespaces = new Namespaces(coreApi);
  const deployments = new Deployments(appsApi)
  
  ipcMain.on('list-namespaces', (event, arg) => {
    namespaces.getNamespaces().then((ns) => {
      event.reply('data', ns)
    })
  })

  ipcMain.on('get-deployments-by-namespace', (event, arg) => {
    deployments.getDeploymentsByNamespace(arg.namespace).then((deploys) => {
      event.reply('response-deployments-by-namespace', deploys)
    })
  })
}

app.whenReady().then(() => {
  createWindow();

  app.on("activate", function () {
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

app.on("window-all-closed", function () {
  if (process.platform !== "darwin") app.quit();
});